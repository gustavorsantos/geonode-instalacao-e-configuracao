# Geonode 

## 1. Instalação

```
git clone https://github.com/GeoNode/geonode-project.git -b 3.x

# Ubuntu
source /usr/share/virtualenvwrapper/virtualenvwrapper.sh
mkvirtualenv --python=/usr/bin/python3 my_geonode
pip install Django==2.2.12

# CentOS
virtualenv -p python3 my_geonode
source my_geonode/bin/activate

# Start project
django-admin startproject --template=./geonode-project -e py,sh,md,rst,json,yml,ini,env,sample -n monitoring-cron -n Dockerfile my_geonode

```

OBS: Se o comando django-admin startproject não funcionar utilizar: 
```
python -m django startproject --template=./geonode-project -e py,sh,md,rst,json,yml,ini,env,sample -n monitoring-cron -n Dockerfile my_geonode
```

## 2. Configuração

### Arquivo .env
```
CREATE_LAYER=False              # Mudar para True
```

## 3. Contêineres

```
sudo nano docker-build.sh

# Comentar a linha caso não queira remover contêineres
# docker system prune -a 

sudo bash docker-build.sh
```
O script realizará o build é iniciará os contêineres.
Geonode disponível em http://localhost e GeoServer em http://localhost/geoserver

```
# No diretório do geonode
docker-compose up -d        # para inciar
docker-compose down         # para parar
```

## 4. Utilizando o Geonode

Acesse **http://localhost**.

Realize o login utlizando as credenciais user: admin password: admin

### Subindo Raster

No menu Data > Upload Layer

Importe o .TIFF do raster e aguarde o upload terminar. Após concluído, ficará disponível no menu Data > Layers

### Criando Camada Vetorial

No menu Data > Create Layer

Preencha os campos, adicionar atributo é opcional. Após a conclusão do upload, será redirecionada a página para a camada.

### Editando Camada Vetorial

No menu direito, clique em Editing Tools > Layer > Edit Data.

Clique no lápis.

![image](/uploads/1f3e0e33de3c983f366aabbb32b46462/image.png)

Crie um novo feature.

![image](/uploads/ca7c3ffa2625dc36d0c1b3c2305f43dc/image.png)

Selecione a ferramenta de lápis e desenhe no mapa a geometria, para ponto basta apenas clicar no lugar desejado. 

![image](/uploads/9ab21a747b8e8964ae49c2d7704d4a68/image.png)

Clique no ícone de Salvar. 

![image](/uploads/3a40dc9194ce019bb2e0ae28b8259add/image.png)

OBS: Caso no processo de criar a camada aconteça algum tipo de erro uma alternativa é utilizar o QGIS para criar um SHP e importar no Geonode pelo mesmo processo do tópico anterior.

### Criando mapa

No menu Maps > Create Map

Clique no ícone de Camadas no canto superior esquerdo.

![image](/uploads/1c2de3ccaf397725868969f78edbf034/image.png)

Adicionar Camada. 

![image](/uploads/981264412309bbf5db8645f4f20f7a4e/image.png)

Abrirá um menu à direita com as camadas disponíveis (criadas e importadas). Clique no símbolo de + das camadas que deseja adicionar ao mapa. E o feche.

![image](/uploads/2efe54d8e86086711630f99c2a863e56/image.png)

No menu esquerdo arraste as camadas para mudar a ordem de exibição.

![image](/uploads/10a0b0ef79d59d6e80a47cb06f842bcc/image.png)

Clique no ícone de Opções no canto superior direito (fundo azul), e salve o mapa.

![image](/uploads/3e09d35f8d92032d92bcc287e3e589b1/image.png)

Acesse o menu Maps > Explore maps e verifique se o mapa criado está listado

## 5. Permissões

No GeoNode podemos gerenciar as permissões de 2 modos, a partir de usuários específicos ou a partir de grupos.
+ Quem pode ver? 
+ Quem pode baixar?
+ Quem pode editar os metadados?
+ Quem pode editar os dados dessa camada?
+ Quem pode editar os estilos dessa camada?
+ QUem pode gerenciar (atualizar, deletar, mudar permissões, publicar e remover publicação)

## 6. Uso 

### Mudar um ponto de lugar

No menu Data > Layer

Selecione a camada do ponto

No menu direito clique em Editing Tools

![image](/uploads/fb89bfce03b786ab8990e70f6a830abd/image.png)

Logo após Edit Data em Layer

![image](/uploads/bdd7a6e1509c889ef7a6c9f32e5cb62d/image.png)

No menu inferior, procure a cada do ponto e a selecione

![image](/uploads/b65c4640da6360c884081f59dd12b84a/image.png)

Após selecionada clique no lápis, será aberto um novo menu, clique para deletar a geometria existente

![image](/uploads/aed2d6dc2bdca0e23dd83967600d5f23/image.png)

Após deletado, clique no lápis com o símbolo de + 

![image](/uploads/a99811e840fc2eb7b6500cf02337568c/image.png)

Clique no mapa, no local onde deseja mover o ponto, e depois clique no ícone de salvar.


